/*
    Code: Accounting Succinctly from SyncFusion
    Purpose: Journal processing functions
    Migration to postgresql: Ivan Candelas <E3 Consultores>
*/

DROP FUNCTION IF EXISTS AddTransaction(VARCHAR(1000), CHAR(2));

CREATE OR REPLACE FUNCTION AddTransaction(
    AcctList VARCHAR(1000), -- Comma Separated: Format is AcctNum|D or C|Amount,
    JournalType CHAR(2) = 'GJ'
) RETURNS INTEGER 
AS $$
    DECLARE
        DECLARE nCtr INTEGER := 0;
        DECLARE DebitTot DECIMAL(12,2);
        DECLARE CreditTot DECIMAL(12,2);
        DECLARE nNext INT;
    BEGIN
        -- Split the parameter into a table
              DROP TABLE IF EXISTS TransTable;
        CREATE LOCAL TEMP TABLE TransTable (AccoutNum VARCHAR(12),ID INT,DC CHAR(1),amt DECIMAL(12,2));
        INSERT INTO TransTable SELECT * FROM TransToTable(AcctList); 
                
                -- Validate all accounts, return -1 if any invalid accounts
        nCtr := (SELECT COUNT(*) FROM TransTable WHERE ID < 0 OR ID is null);
        IF nCtr > 0  THEN
            -- Optionally, could raise an error
            RAISE NOTICE 'Missing account numbers';
            RETURN -1;
        END IF;
        
                -- Validate Debits = Credits, return -2 if not
        DebitTot = (SELECT SUM(amt) FROM TransTable WHERE DC='D');
        CreditTot = (SELECT SUM(amt) FROM TransTable WHERE DC='C');
        
        IF DebitTot <> CreditTot THEN
            -- Optionally, could raise an error
            RAISE NOTICE 'Debits <> Credits';
            RETURN -2;
        END IF;
        
                -- Post the transaction into journals
        nNext = (SELECT COALESCE(max(transNum)+1,1) FROM Journals WHERE Journals.jrnlType=JournalType);
        
        INSERT INTO Journals (AccountID,JrnlType,TransNum,DC,Amount) SELECT ID,JournalType,nNext,DC,amt FROM TransTable;

        RETURN 0;
    END
$$ language 'plpgsql';


DROP FUNCTION IF EXISTS TransToTable(VARCHAR(1000));

CREATE OR REPLACE FUNCTION TransToTable (AcctList VARCHAR(1000) )
RETURNS TABLE(
    AcctNumber VARCHAR(12),
    Jrnl_Account_ID INT,
    DebitCredit CHAR(1),
    Amt DECIMAL(12,2)) 
AS $$
    DECLARE
        X integer := 0;
        Y integer := 0;
        OneLine varchar(30);
        DebCred CHAR(1);
        TransAmt DECIMAL(12,2);
        AcctNUM VARCHAR(12);
        AcctID integer := 0;
        rec record;
    BEGIN
    AcctList := AcctList || ',';
    X := position(',' in AcctList);
    LOOP 
      EXIT WHEN X <= 0 ; 
      OneLine = LEFT(AcctList,X-1);
      AcctList := RTRIM(SUBSTRING(AcctList,X+1,9999));
      IF length(OneLine) > 0 THEN
           Y := position('|' in OneLine);
        AcctNum = LEFT(OneLine,Y-1);
                DebCred = SUBSTRING(OneLine,Y+1,1);
                OneLine = RTRIM(SUBSTRING(OneLine,Y+3,9999));
        TransAmt = CAST(OneLine AS DECIMAL(12,2));
        AcctID := (SELECT id FROM chart_of_accounts xx WHERE xx.accountNum=AcctNum);
        RETURN QUERY select AcctNum,AcctID,DebCred,TransAmt;
      END IF;
      X := position(',' in AcctList);
    END LOOP ; 
    END
$$ language 'plpgsql';


DROP FUNCTION IF EXISTS PostTransaction(INT);

CREATE OR REPLACE FUNCTION PostTransaction( TransNumb INT = 0 )
RETURNS INTEGER
AS	$$
    BEGIN
        UPDATE Chart_of_Accounts SET Balance = Balance + xx.PostAmt FROM
        ( SELECT AccountID, Sum( CASE WHEN jl.dc='D' THEN amount ELSE -1*amount END ) as PostAmt
        FROM Journals jl JOIN Chart_of_Accounts ca on jl.AccountID=ca.id
        WHERE jl.posted='N' AND ca.AcctType in ('A','E')
        AND (Transnum = TransNumb or TransNumb=0)
        GROUP BY AccountID
        ) xx
        WHERE xx.accountID=ID;
        UPDATE Chart_of_Accounts SET Balance = Balance +xx.PostAmt FROM
        ( SELECT AccountID,Sum(CASE WHEN jl.dc='C' THEN amount ELSE -1*amount END) as PostAmt
        FROM Journals jl JOIN Chart_of_Accounts ca on jl.AccountID=ca.id
        WHERE jl.posted='N' AND ca.AcctType in ('L','O','R')
        AND (Transnum = TransNumb or TransNumb=0)
        GROUP BY AccountID
        ) xx
        WHERE xx.accountID=ID;
        UPDATE Journals SET posted='Y',PostDate=current_timestamp
        WHERE posted='N' AND (Transnum = TransNumb or TransNumb=0);
        RETURN 0;
    END
$$ language 'plpgsql';
