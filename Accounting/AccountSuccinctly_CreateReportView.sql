/*
    Code: Accounting Succinctly from SyncFusion
    Purpose: Reports
    Migration to postgresql: Ivan Candelas <E3 Consultores>
*/

DROP VIEW IF EXISTS BalanceSheet;

-- Balance Sheet
CREATE VIEW BalanceSheet
AS
    SELECT AccountNum,Descrip,Balance FROM Chart_of_Accounts WHERE AcctType='A'
    UNION
    SELECT '1900','TOTAL ASSETS',Sum(Balance) FROM Chart_of_Accounts WHERE AcctType='A'
    UNION
    SELECT AccountNum,Descrip,Balance FROM Chart_of_Accounts WHERE AcctType='L'
    UNION
    SELECT '2900','TOTAL LIABILITIES',Sum(Balance) FROM Chart_of_Accounts WHERE AcctType='L'
    UNION
    SELECT AccountNum,Descrip,Balance FROM Chart_of_Accounts WHERE AcctType='O'
    UNION
    SELECT '3900','TOTAL EQUITY',Sum(Balance) FROM Chart_of_Accounts WHERE AcctType='O'
    UNION
    SELECT '3999','TOTAL LIABILITIES/EQUITY',Sum(Balance) FROM Chart_of_Accounts WHERE AcctType IN ('L','O');


DROP VIEW IF EXISTS IncomeStatement;

--Income Statement
CREATE VIEW IncomeStatement AS
    SELECT '4000' as Seq,'REVENUE' as "Account Name",COALESCE(Sum(jl.Amount),0) as Balance
    FROM Journals jl
    JOIN Chart_of_Accounts ca on ca.id=jl.AccountId
    WHERE jl.posted='N' and ca.AcctType='R'
    UNION
    SELECT ca.AccountNum, descrip, COALESCE(Sum(jl.Amount),0) as Balance
    FROM Journals jl
    JOIN Chart_of_Accounts ca on ca.id=jl.AccountId
    WHERE jl.posted='N' and ca.AcctType='E'
    GROUP BY ca.descrip,ca.AccountNum
    UNION
    SELECT '9999','NET INCOME(loss)',xx.Balance
    FROM (
    SELECT COALESCE(Sum(CASE when jl.dc='D' then -1*jl.amount else jl.amount end),0 ) as Balance
    FROM Journals jl
    JOIN Chart_of_Accounts ca on ca.id=jl.AccountId
    AND jl.posted='N' and (ca.AcctType IN ('R','E'))
    ) xx;


