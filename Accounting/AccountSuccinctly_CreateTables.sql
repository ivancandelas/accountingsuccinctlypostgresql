/*
    Code: Accounting Succinctly from SyncFusion
    Purpose: Generate data tables
    Migration to postgresql: Ivan Candelas <E3 Consultores>
*/

DROP TABLE IF EXISTS Journals;
DROP TABLE IF EXISTS Chart_of_Accounts;

CREATE TABLE Chart_of_Accounts
(
  ID SERIAL,
  AccountNum VARCHAR(12) UNIQUE NOT NULL,
  Descrip VARCHAR(48),
  AcctType CHAR(1) CHECK (AcctType in ('A','L','O','R','E')),
  Balance DECIMAL(12,2),
  CONSTRAINT PK_Chart_of_Accounts PRIMARY KEY (ID)
);

CREATE TABLE Journals
( 
  ID SERIAL, -- Unique key per line item
  AccountID INT,
  JrnlType CHAR(2),     -- GJ, AR, AP, SJ, PJ, etc.
  TransNum INT,         -- Key to group entries together.
  DC CHAR(1) CHECK (DC in ('D','C')),
  Posted CHAR(1) DEFAULT 'N',
  TransDate TIMESTAMP DEFAULT current_timestamp,
  PostDate TIMESTAMP,
  Amount DECIMAL(12,2) NOT NULL,
  CONSTRAINT PK_Journals PRIMARY KEY (ID),
  CONSTRAINT FK_Chart FOREIGN KEY (AccountID) REFERENCES Chart_of_Accounts(ID)
);

